WPCONTENT=/var/www/demo.graphpaperpress.com/public_html/wp-content
SLUG=onesie
echo "Deployment started for $SLUG"
cd $WPCONTENT/themes/$SLUG
git pull origin master
chown -R www-data:www-data .
git archive --format=zip -o $WPCONTENT/downloads/$SLUG.zip HEAD . ":(exclude)bitbucket-pipelines.yml" . ":(exclude)deploy.sh" . ":(exclude).gitignore"
chown www-data:www-data $WPCONTENT/downloads/$SLUG.zip
echo "Deployment finished for $SLUG"